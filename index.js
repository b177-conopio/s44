// Get post data
// Fetch data from server in json, then trigger showPost to see the data received from server
fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => showPosts(data));

// Add post data
// html 10 and 15, submit will execute an event
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault();

	// {} is an argument, show what data will we get
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	// promise response in json format
	.then((response) => response.json())
	// promise to see data in console log
	.then((data) => {
		console.log(data);
		alert('Successfully added')

	// to erase text in text body after adding
	document.querySelector('#txt-title').value = null;
	document.querySelector('#txt-body').value = null;
	})
})

// Show posts

// (posts) is parameter
const showPosts = (posts) => {
	let postEntries = '';

	// post is individual
	posts.forEach((post) =>{
		// what we see is data that we fecth from server
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit post
const editPost = (id) => {
	// title we'll get from post html file
	// targeting 44 and 45
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
}

// Edit post
document.querySelector('#form-edit-post').addEventListener('submit', (e) =>{
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	// return response into json format
	.then((response) => response.json())
	// what will reflect in console
	.then((data) => {
		console.log(data);
		alert('Successfully updated');

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		document.querySelector('#btn-submit-update').setAttribute('disabled', true);

	})
})

// Delete post
const deletePost = (id) => {
    document.querySelector(`#post-${id}`).remove();
    alert('Successfully deleted');
}

